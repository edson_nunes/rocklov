describe "POST /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@bol.com.br", password: "1234" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24
      #24 é o tamanho padrao do id no mongoDb
    end
  end

  context "usuario ja existe" do
    before(:all) do
      # dado que eu tenho um novo usuario
      payload = { name: "Edson", email: "edson@gmail.com", password: "1234" }
      MongoDB.new.remove_user(payload[:email])

      #e o email deste usuario ja foi cadastrado no sistema
      Signup.new.create(payload)

      # quando faço uma requisição para a rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      # entao deve retornat 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  examples = [
    {
      title: "sem senha",
      payload: { name: "Edson", email: "edson@gmail.comm", password: "" },
      code: 412,
      error: "required password",
    },
    {
      title: "sem campo senha",
      payload: { name: "Edson", email: "edson@gmail.com" },
      code: 412,
      error: "required password",
    },
    {
      title: "sem email",
      payload: { name: "Edson", email: "", password: "123456" },
      code: 412,
      error: "required email",
    },
    {
      title: "sem campo email",
      payload: { name: "Edson", password: "123456" },
      code: 412,
      error: "required email",
    },
    {
      title: "sem nome",
      payload: { name: "", email: "edson@gmail.comm", password: "123456" },
      code: 412,
      error: "required name",
    },
    {
      title: "sem campo nome",
      payload: { email: "edson@gmail.comm", password: "123456" },
      code: 412,
      error: "required name",
    },
  ]

  examples.each do |e|
    context "#{e[:title]}" do
      before (:all) do
        @result = Signup.new.create(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do usuario" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
