# conceito DRY Don't Repeat Yourself = Não se repita (utiliza o encapsulamento)
# para testes é mais importante a clareze do que o DRY, por isso sera alterado o argumento do metodo login para receber o payload completo
# assim trabalhamos com a massa de teste explicita na camada de script, que é uma boa pratica :)

describe "POST /sessions" do
  context "login com sucesso" do
    before (:all) do
      payload = { email: "betao@hotmail.com", password: "1234" }
      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24
      #24 é o tamanho padrao do id no mongoDb
    end
  end

  # para rodar com massa de teste em arquivo separado  (metodo Helpers criado no arquivo helpers.rb)
  examples = Helpers::get_fixture("login")

  #   examples = [
  #     {
  #       title: "senha incorreta",
  #       payload: { email: "betao@gmail.com", password: "123456" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },
  #     {
  #       title: "usuario não existe",
  #       payload: { email: "404@gmail.com", password: "1234" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },
  #     {
  #       title: "email vazio",
  #       payload: { email: "", password: "123456" },
  #       code: 412,
  #       error: "required email",
  #     },
  #     {
  #       title: "sem campo email",
  #       payload: { password: "123456" },
  #       code: 412,
  #       error: "required email",
  #     },
  #     {
  #       title: "senha vazia",
  #       payload: { email: "betao@gmail.com", password: "" },
  #       code: 412,
  #       error: "required password",
  #     },
  #     {
  #       title: "sem campo senha",
  #       payload: { email: "betao@gmail.com" },
  #       code: 412,
  #       error: "required password",
  #     },
  #   ]

  examples.each do |e|
    context "#{e[:title]}" do
      before (:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do usuario" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
