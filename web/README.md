

instalar:

Visual Studio Code - pelo ubuntu memso
plugins recomendados: one dark darker
                      material icon theme
                      Cucumber (Gherkin) Full Support
                      rufo (Ruby formatter) identação automatica para ruby (opcional)  necessario executar no terminal: gem install rufo



chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb

Instalação do curl:
-sudo apt update
-sudo apt install curl
-curl --version

sudo apt install git

para customizar o terminal:

https://medium.com/@ivanaugustobd/seu-terminal-pode-ser-muito-muito-mais-produtivo-3159c8ef77b2

apt install zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

plugins
 1- zsh-syntax-highlighting: basicamente vai deixar o comando verde se tiver sido digitado corretamente, ou do contrário, vermelho
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting


2- zsh-autosuggestions: vai sugerir comandos baseados no seu histórico.
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions

Daí adicione eles na lista de plugins do seu “~/.zshrc”
sudo nano ~/.zshrc

************************************************************************************************************

# Instalar Ruby
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev

git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(rbenv init -)"' >> ~/.zshrc

git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.zshrc

source ~/.zshrc

rbenv install 2.6.3
rbenv global 2.6.3

source ~/.zshrc

************************************************************************************************************

Instalar docker e docker-compose


Instalação do Docker:
-sudo apt update
-sudo apt install docker.io
-docker --version

Instalação do Docker Compose:
-sudo apt update
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
Para instalar uma versão diferente do Compose, substitua 1.27.4 pela versão do Compose que deseja usar.

-sudo chmod +x /usr/local/bin/docker-compose
-docker-compose --version

Removendo sudo do docker:
-sudo usermod -aG docker $USER
-Necessário reiniciar o Ubuntu

Documentação Docker Compose:
https://docs.docker.com/compose/install

Releases Docker Compose:
https://github.com/docker/compose/releases


************************************************************************************************************

Subir containers do projeto:

entrar na pasta /rocklov-dc/versao  (r1,r2,r3. etc...)

executar:      docker-compose up -d
verificar com: docker ps

## editar arquivo hosts

sudo nano /etc/hosts

adicionar:
127.0.0.1 rocklov-db
127.0.0.1 rocklov-api
127.0.0.1 rocklov-web

estando correto agora ja é possivel acessar o ambiente

http://rocklov-web:3000
http://rocklov-api:3333/api-docs


# instalando as dependencias do projeto

gem install bundler
bundle init -  para criar o arquivo Gemfile  (arquivo de dependencias)

cucumber - identifica os cenário

acessar o site: rubygems.org   
pesquisar cucumber, copiar o comando para utilizar no gemfile

bundle install (le o arquivo Gemfile, e instala as dependencias mapeadas) * sempre rodar a cada dependencia adicionada*

conferir: cucumber --version
 **no meu caso nao reconheceu o comando e fo necessário dar permissão na pasta: chmod -R 777 /home/edson/qaninja/a180/rocklov 

 cucumber --init   - cria a estrutura básica

 Capybara - automatiza os testes para aplicações web, simulando o comportamento do usuário
 bundle install 

 após, configurar no arquivo env.rb:

require "capybara"
require "capybara/cucumber"


Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.app_host = "http://rocklov-web:3000"
    config.default_max_wait_time = 10
end

- instalar a Gem do selenium   (gem 'selenium-webdriver', '3.142.7')

baixar o driver do selenium 
- verificar versao do chrome instalada: Versão 88.0.4324.96
- acessar: https://chromedriver.storage.googleapis.com/index.html pesquisar  88.0.4324 (se desconsidera o .96 pois é o nuemro de builds da versão)
-baixar chromedriver_linux64.zip

No Linux é só executar os comandos abaixo:

unzip chromedriver_linux64.zip
sudo mv chromedriver /usr/bin/chromedriver
sudo chown root:root /usr/bin/chromedriver
sudo chmod +x /usr/bin/chromedriver

## acessar o banco MongoDb

é possivel instalar pela  busca no ubuntu tmb!

site https://robomongo.org/download
extrair e executar o bin
/home/edson/Downloads/robo3t-1.4.2-linux-x86_64-8650949/bin

criar nova conexão:
nome: RockLov
adress: rocklov-db    porta: 27017   (configurada no arquivo 'docker-compose.yml' da pasta rocklov-dc/r3')

para visualisar o conteudo do arquivo de config:  cat docker-compose.yml


# allure Framework  (para os relátórios de execução)

documentação
https://docs.qameta.io/allure/

https://rubygems.org/gems/allure-cucumber

adicionar no gemfile
executar bundle install

-no arquivo env.rb:
 require "allure-cucumber"

 AllureCucumber.configure do |config|
  config.results_directory = "/logs"
  config.clean_results_directory = true #limpa a pasta logs a cada execução
end

-no arquivo cucumber.yml
adicionar novo profile

allure: --format AllureCucumber::CucumberFormatter --out=logs

# criar servidor de relatórios

sudo apt-add-repository ppa:qameta/allure
sudo apt-get update 
sudo apt-get install allure

-Instalar JAVA

sudo apt install openjdk-8-jdk
java -version

-Criar variavel ambiente
buscar onde foi instalado
sudo update-alternatives --config java
saida do comando: Existe apenas uma alternativa no grupo de ligação java (que disponibiliza /usr/bin/java: /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java

editar o arquivo:
sudo nano /etc/environment

JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java"    *****(remover o final /bin/java)

salvar e executar:
source /etc/environment

para verificar:
echo $JAVA_HOME 
-deve retornar o caminho configurado
"/usr/lib/jvm/java-8-openjdk-amd64/jre"

Instalar container Jenkins

https://medium.com/qacademy/rodando-o-jenkins-em-um-container-docker-4772986eb801


iniciar docker jenkins

docker ps -a
docker start <id_container>











