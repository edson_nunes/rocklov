Então("sou redirecionado para o Dashboard") do
  expect(@dash_page.on_dash?).to be true
end

# codígo otimizado, utilizando argumentos no cucumber sinalizados entre "" no cadastro.feature,
#assim todas as validações de msg de alerta são comparadas com o conteudo do argumento |expect_alert|
Então("vejo a mensagem de alerta: {string}") do |expect_alert|
  expect(@alert.dark).to eql expect_alert
end
