Dado("que acesso a página de cadastro") do
  @signup_page.open
end

Quando("submeto o seguinte formulário de cadastro:") do |table|
  # table is a Cucumber::MultilineArgument::DataTable

  #log table.hashes
  user = table.hashes.first  #.hashes converte a tabela para array - .firs pega o primeiro componente do array
  #log user

  MongoDB.new.remove_user(user[:email])

  @signup_page.create(user)
end
