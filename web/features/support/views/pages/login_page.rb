class LoginPage
  #include é como uma herança, informando o ruby que é necessário executar os metodos do Capybara( visit "/"), com o comando abaixo a classe do ruby reconhece os recursos do Capybara
  include Capybara::DSL

  def open
    visit "/"
  end

  def with(email, password)
    find("input[placeholder='Seu email']").set email
    find("input[type=password]").set password
    click_button "Entrar"
  end
end
