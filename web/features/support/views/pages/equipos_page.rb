class EquiposPage
  include Capybara::DSL

  def create(equipo)
    #expect(page).to have_css "#equipoForm"  isso é um checkpoint para garantir que estou no lugar correto (proibido usar 'expect' dentro do pageObject, viola a arquitetura)
    page.has_css?("#equipoForm") #metodo com '?' retorna verdadeito ou falso, assim é feito um checkpoint utilizando o time_out do Capybara para aguardar o elemento na tela

    upload(equipo[:thumb]) if equipo[:thumb].length > 0

    find("input[placeholder$=equipamento]").set equipo[:nome]
    #input[placeholder$=equipamento]  $ pega a ultima palavra
    #input[placeholder^=]  $ pega a primeira palavra
    #input[placeholder*=]  $ contenha a  palavra
    select_cat(equipo[:categoria]) if equipo[:categoria].length > 0
    find("input[placeholder^=Valor]").set equipo[:preco]

    click_button "Cadastrar"
  end

  #metodo criado para não dar erro no cenario ' Tentativa de cadastro anúncios' (sem categoria)
  def select_cat(cat)
    find("#category").find("option", text: cat).select_option
  end

  #metodo criado para não dar erro no cenario ' Tentativa de cadastro anúncios' (Anúncio sem foto)'
  def upload(file_name)
    thumb = Dir.pwd + "/features/support/fixtures/images/" + file_name
    find("#thumbnail input[type=file]", visible: false).set thumb
  end
end
