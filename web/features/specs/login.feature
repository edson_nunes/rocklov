#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "edsonnunes@gmail.com" e "1234"
        Então sou redirecionado para o Dashboard


    Esquema do Cenário: Tentar logar

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
        |email_input         |senha_input|mensagem_output|
        |edson@gmail.com     |4321       |Usuário e/ou senha inválidos.|
        |edsonnunes@404.com  |1234       |Usuário e/ou senha inválidos.|
        |nunes$gmail.com     |1234       |Oops. Informe um email válido!|
        |                    |1234       |Oops. Informe um email válido!|
        |edson@gmail.com     |           |Oops. Informe sua senha secreta!|

